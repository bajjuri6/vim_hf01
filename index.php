<html ng-app="mkApp" ng-cloak>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <title>MK</title>
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">
        <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">        

        <!-- My Style Sheets -->
        <link href="css/mk.css" rel="stylesheet" type="text/css"/>
        <link href="css/flex-calendar.css" rel="stylesheet" type="text/css"/>

        <!-- Angular Material requires Angular.js Libraries -->
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-aria.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-messages.min.js"></script>
        <script src="js/angular-translate.min.js" type="text/javascript"></script>
        <script src="js/flex-calender.js" type="text/javascript"></script>
<!--        <script src="//cdnjs.cloudflare.com/ajax/libs/angular-translate/2.11.1/angular-translate.min.js"></script>-->

        <!-- Jquery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="//cdn.rawgit.com/nnattawat/flip/master/dist/jquery.flip.min.js"></script>
        <script src="js/sly.min.js" type="text/javascript"></script>        

        <!-- Angular Material Library -->
        <script src="//ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.js"></script>
    </head>
    <body>
        <div ng-controller="mkCtrl">
            <div layout="row" layout-align="space-between start" class="navbar width_100" ng-if="sh_d" layout-xs layout-align-xs="space-between center">
                <div layout flex-offset="5" flex="15">
                    <img ng-src="images/logo-MK.png" class="logo">
                </div>
                <div layout flex flex-sm layout-align="start center">
                    <ul layout flex>
                        <li flex><a href="#home" target="_self" class="active">HOME</a></li>
                        <li flex><a href="#about" target="_self">ABOUT US</a></li>
                        <li flex><a href="#brands" target="_self">BRANDS</a></li>                            
                        <li flex><a href="#events" target="_self">EVENTS</a></li>
                        <li flex><a href="#contact" target="_self">CONTACT</a></li>
                    </ul>
                </div>
            </div>
            <div class="header" ng-if="sh_m">
                <div>
                    <a><img ng-src="images/logo-MK.png" class="logo"></a>
                </div>
                <div>
                    <i class="material-icons icon-menu ">&#xE5D2;</i>
                    <ul class="menu">
                        <li><a href="#home" class="outline">Home</a></li>
                        <li><a href="#about" class="outline">About Us</a></li>
                        <li><a href="#brands" class="outline">Brands</a></li>                            
                        <li><a href="#events" class="outline">Events</a></li>
                        <li><a href="#contact" class="outline">Contact</a></li>
                    </ul>
                </div>
            </div>
            <div layout="column" layout-align="start stretch" layout-fill class="home" id="home">                                                
                <div layout layout-align="center stretch" layout-xs="column" flex="100" class="aftrNav">
                    <div flex="50" flex-xs="50" flex-order-xs="2" flex-offset="5" layout="column" layout-align="center start">
                        <div layout="column" layout-align="start start">
                            <h3 class="mkH3">M KITCHENS</h3>
                            <h2 class="stbh">setting the bar high</h2>
                            <p class="txtp">
                                Vegetables are essential to the human diet.We get a great deal of our daily vitamin content from vegatables.
                            </p>
                        </div>
                        <div class="explore " layout layout-align="center center">
                            <a href="#about" class="outline">EXPLORE</a>
                        </div>
                    </div>
                    <div flex="50" flex-xs="50" flex-order-xs="1" layout="row" layout-align="space-around stretch" layout-xs layout-align-xs="space-around stretch">
                        <div layout="column" layout-align="center stretch" flex="45">
                            <div flex="30" flex-xs="33" class="brndImg" id="hylife" layout layout-align="center center"><img  src="images/logo-hylife.png"></div>
                            <div flex="30" flex-xs="33" class="brndImg" id="elixir" layout layout-align="center center"><img  src="images/logo-elixir.png"></div>
                            <div flex="30" flex-xs="33" class="brndImg" id="bon" layout layout-align="center center"><img  src="images/logo-bon.png"></div>
                        </div>
                        <div layout="column" layout-align="center stretch" flex="45">
                            <div flex="30" flex-xs="33" class="brndImg" id="canoli" layout layout-align="center center"><img  src="images/logo-canoli.png"></div>
                            <div flex="30" flex-xs="33" class="brndImg" id="currie" layout layout-align="center center"><img  src="images/logo-currie.png"></div>
                            <div flex="30" flex-xs="33"></div>                            
                        </div>                        
                    </div>
                </div>
            </div>
            <div layout layout-align="center center" class="is_rltv abt">
                <div id="about" class="md-whiteframe-10dp" layout layout-xs="column">
                    <div layout flex="30" layout-xs="column" flex-xs="40">
                        <img src="images/img-aboutus.png" class="width_100" ng-if="sh_d">
                        <img src="images/img-about-us.png" ng-if="sh_m">
                    </div>
                    <div layout flex="70" layout-xs="column" flex-xs="100">                                            
                        <div  class="md-padding">
                            <div class="md-padding">
                                <h1 class="gtkh">GET TO KNOW</h1>
                                <h2 class="abth">about us</h2>
                            </div>                                
                            <div class="txtabt md-padding">
                                <div class="md-padding" hide show-xs></div>
                                <p>
                                    M. Kitchens is a hospitality group in the United States, looking to expand base in India.
                                </p>
                                <p>
                                    We aim to expand to over 100 locations in India itself within the next ten years.
                                </p>
                                <p>
                                    Hylife Brewing Company @ 800 Jubilee is the first venture of many to come. Next up, also at 800 Jubilee, are Mirchi (South Indian restaurant), Ibachi Hut (India's first Teppanyaki restaurant) and Cannoli Cafe (bistro).
                                </p>
                                <p>
                                    We will also offer catering services for office parties, weddings, private and outdoor events and plan on partnering with various other hospitality groups to bring concepts from the US and international standards to the Indian ecosystem.
                                </p>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <div layout class="bg-seperator-1"></div>            
            <div layout layout-align="center center" class="is_rltv">
                <div id="brands" class="md-whiteframe-10dp" layout="row" layout-xs="column">
                    <div layout="column" flex="50" flex-xs="100" class="brdrRight">
                        <div layout="column" flex-offset="5" class="md-padding"  layout-align="center start" flex="50" flex-xs="100">
                            <div class="md-padding">
                                <h1 class="our">OUR</h1>
                                <h2 class="brndh">brands</h2>
                            </div>
                            <div class="md-padding txtBrnds" layout layout-align="start center">
                                <p>
                                    The Dietary Guidelines for Americans 2010 recommend you make one-half of your plate fruits and vegetables. 
                                </p> 
                            </div>                            
                        </div>
                        <div layout layout-align="center stretch" flex="50" flex-xs="100">
                            <div layout layout-align="center stretch" layout-align-xs="center center" flex="50">
                                <img src="images/img-brands.png" flex="100">
                            </div>
                            <div layout="column" layout-align="center center" flex="50" class="brdrTop cursor_point flip">
                                <div class="front" layout layout-align="center center">
                                    <img src="images/logo-hylife-brown.png" flex-xs="75">
                                </div>
                                <div class="back" layout layout-align="center center">
                                    <span>some text here</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div layout="row" flex="50" flex-xs="100">
                        <div layout="column"  flex="50" flex-xs="100" >
                            <div layout="column" layout-align="center center" flex="50" class="cursor_point flip">
                                <div class="front" layout layout-align="center center">
                                    <img src="images/logo-currie-brown.png" flex-xs="75">
                                </div>
                                <div class="back" layout layout-align="center center">
                                    <span>some text here</span>
                                </div>
                            </div>
                            <div layout="column" layout-align="center center" flex="50" class="brdrTop cursor_point flip">                                
                                <div class="front" layout layout-align="center center">
                                    <img src="images/logo-canoli-brown.png" flex-xs="75">
                                </div>
                                <div class="back" layout layout-align="center center">
                                    <span>some text here</span>
                                </div>
                            </div>
                        </div>
                        <div layout="column" layout-xs layout-align-xs="center center" flex="50" flex-xs="100" >
                            <div layout="column" layout-align="center center" flex="50" class="brdrLeft cursor_point flip">                                
                                <div class="front" layout layout-align="center center">
                                    <img src="images/logo-bon-brown.png" flex-xs="75">
                                </div>
                                <div class="back" layout layout-align="center center">
                                    <span>some text here</span>
                                </div>
                            </div>
                            <div layout="column" layout-align="center center" flex="50" class="brdrLeft brdrTop cursor_point flip">                                
                                <div  class="front" layout layout-align="center center">
                                    <img src="images/logo-elixir-brown.png" flex-xs="75">
                                </div>
                                <div class="back" layout layout-align="center center">
                                    <span>some text here</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div layout class="bg-seperator-2"></div>
            <div layout layout-align="center center" class="is_rltv">                
                <div id="events" layout="column" layout-align="center center" class="md-whiteframe-10dp">                   
                    <div layout>                    
                        <div class="brdrRight " layout="column" flex="60" flex-sm="55" flex-xs="100" layout-align="center start">
                            <div layout="row" layout-align="space-between start" class="width_100">
                                <div layout="column" class="md-padding">
                                    <h1 class="our">UPCOMING</h1>
                                    <h2 class="events">events</h2>
                                </div>
                                <div ng-if="sh_m" flex-xs layout layout-align="end center">
                                    <md-datepicker ng-model="myDate" ng-change="dateClick()"></md-datepicker>
                                </div>
                            </div>
                            <div class="frame_prnt" layout layout-align="space-between center">
                                <div id="slyFrame" class="frame">
                                    <ul class="slidee">
                                        <li ng-repeat="similarEvent in similarEvents" class="cursor_point outline"  ng-click="upcomingEvents($event)">
                                            <span class="evnt_lst_sub_h">{{similarEvent.sub_hd}}</span>
                                            <div class="evnt_lst_h">
                                                <h4>{{similarEvent.main_hd}}</h4>
                                                <div class="icon_and_time">
                                                    <span><i class="material-icons icon-schedule">&#xE8B5;</i></span>
                                                    <span class="time">{{similarEvent.time}}</span>
                                                </div>
                                            </div>
                                        </li>                                        
                                    </ul>                                
                                </div>
                                <div class="scrollbar">
                                    <div class="handle">
                                        <div class="mousearea"></div>
                                    </div>                                
                                </div>    
                            </div>
                        </div>
                        <div layout flex="40" flex-sm="45" flex-xs="45" class="md-padding" ng-if="sh_d">                           
                            <div class="wrapp">
                                <flex-calendar options="options" events="events"></flex-calendar>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            <div layout class="bg-seperator-3"></div>
            <div layout layout-align="center center" class="is_rltv">
                <div layout layout-xs="column" id="contact" class="md-whiteframe-10dp">
                    <div layout="row" flex="25" flex-md="30" flex-sm="30" flex-xs="100" style="position: relative">                        
                        <div layout="column" flex class="bg-white hgtf">
                            <div layout="column" class="getin_touch" layout-margin>                                
                                <h1 class="get_in">GET IN</h1>
                                <h2 class="touch">touch</h2>                                
                            </div>
                            <div layout="column" class="md-padding"  flex="100">                        
                                <form name="contactfrm" flex="100" ng-submit="contactfrm.$valid && custFormData()" novalidate>
                                    <md-input-container class="md-block" md-is-error="contactfrm.cust_name.$invalid && (contactfrm.$submitted || contactfrm.cust_name.$dirty)">
                                        <label>NAME</label>
                                        <input type="text"  ng-required="true" ng-maxlength="35" name="cust_name" ng-model="customer.cust_name" ng-pattern="/^[a-zA-Z\s.@]*$/">
                                        <div ng-messages="contactfrm.cust_name.$error" ng-if="contactfrm.$submitted" role="alert" >
                                            <div ng-message="required">Name is Required.</div>                                            
                                            <div ng-message="maxlength">Name is too long.</div>
                                            <div ng-message="pattern">Name field allowed only these spceial charecters .@</div>
                                        </div>
                                    </md-input-container>
                                    <md-input-container class="md-block" md-is-error="contactfrm.cust_email.$invalid && (contactfrm.$submitted || contactfrm.cust_email.$dirty)">
                                        <label>EMAIL</label>
                                        <input  type="text"  name="cust_email" ng-model="customer.cust_email" ng-required="true"  ng-pattern="/^.+@.+\..+$/" />
                                        <div ng-messages="contactfrm.cust_email.$error" ng-if="contactfrm.$submitted"  role="alert" >
                                            <div ng-message-exp="['required','pattern']">
                                                Please enter a valid e-mail address
                                            </div>
                                        </div>
                                    </md-input-container>          
                                    <md-input-container class="md-block mblno_container" md-is-error="contactfrm.cust_mobileNo.$invalid && (contactfrm.$submitted || contactfrm.cust_mobileNo.$dirty)">
                                        <label id="mblnm_label">PHONE NUMBER</label>
                                        <input id="mobileNo" name="cust_mobileNo" ng-required="true" ng-model="customer.cust_mobileNo" ng-pattern="/^[(]?[0-9]{3}[)]?[-.]?[0-9]{3}[-.]?[0-9]{4,6}$/"  type="text" >
                                        <div ng-messages="contactfrm.cust_mobileNo.$error" ng-if="contactfrm.$submitted">
                                            <div ng-message="required">Mobile Number is required.</div>
                                            <div ng-message="pattern">Please enter a valid Mobile Number</div>
                                        </div>
                                    </md-input-container>                            
                                    <div class="md-padding"></div>
                                    <md-input-container class="md-block" md-is-error="contactfrm.cust_msg.$invalid && (contactfrm.$submitted || contactfrm.cust_msg.$dirty)" >
                                        <label>YOUR MESSAGE</label>
                                        <textarea  name="cust_msg" ng-required="true" ng-model="customer.cust_msg"></textarea>
                                        <div ng-messages="contactfrm.cust_msg.$error" ng-if="contactfrm.$submitted">
                                            <div ng-message="required">Message is Required.</div>                                            
                                        </div>
                                    </md-input-container>                            
                                    <md-input-container layout layout-align-xs="center center">
                                        <md-button type="submit" flex="100" flex-xs="75" class="submit">submit</md-button>
                                    </md-input-container>
                                </form>                                
                            </div>
                        </div>
                        <div class="wao"  layout="column"  layout-align="start center" layout-margin ng-if="sh_d">                        
                            <ul><li>We are open </li></ul>
                            <hr>
                            <span> Sunday to Tuesday 09:00-24:00</span>
                            <span> Friday and Sunday 08:00-03:00</span>
                        </div>
                    </div>
                    <div layout flex="75" flex-md="70" flex-sm="70" flex-xs="100" class="map_pdt" ng-if="sh_d">
                        <div flex="100">
                            <div class="my-map-canvas is_rltv">
                                <iframe frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Sanjeeva+Reddy+Nagar,+Hyderabad,+Telangana,+India&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="my-map-canvas is_rltv" ng-if="sh_m">
                <div class="wao" layout="column" layout-align="start center" layout-margin>                        
                    <ul><li>We are open </li></ul>
                    <hr>
                    <span> Sunday to Tuesday 09:00-24:00</span>
                    <span> Friday and Sunday 08:00-03:00</span>
                </div>
                <iframe frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Sanjeeva+Reddy+Nagar,+Hyderabad,+Telangana,+India&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe>
            </div>
            <div class="ftr" layout="column"  flex>                
                <div  layout="row" class="md-padding" layout-align="end center" layout-align-xs="start center" flex="100">
                    <div layout flex-offset="5" flex="15" flex-sm="10">
                        <img ng-src="images/logo-MK.png" class="logo">
                    </div>
                    <div layout flex flex-sm layout-align="start center" ng-if="sh_d">
                        <ul layout flex>
                            <li flex><a href=""  >HOME</a></li>
                            <li flex><a href=""  >ABOUT US</a></li>
                            <li flex><a href=""  >BRANDS</a></li>
                            <li flex><a href=""  >LOCATION</a></li>
                            <li flex><a href=""  >EVENTS</a></li>
                            <li flex><a href=""  >CONTACT</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Custom Js -->
        <script src="js/mk.js" type="text/javascript"></script>

    </body>
</html>
