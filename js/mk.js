var app = angular.module("mkApp", ['ngMaterial', 'ngMessages','flexcalendar','pascalprecht.translate']);
app.controller('mkCtrl', ['$scope', '$mdDialog', '$http', '$filter','$mdMedia', function ($scope, $mdDialog, $http, $filter, $mdMedia) {

        $(document).ready(function () {
            
            /* set height for about parent container */
            if($(document).width() < 599){
                var biggestHeight = "0";
                $(".is_rltv.abt *").each(function(){
                    if ($(this).height() > biggestHeight ) {
                        biggestHeight = $(this).height();
                    }
                });
                $(".is_rltv.abt").height(biggestHeight);
            }            

            /* Target to Div */
            $('a[href^="#"]').on('click', function (event) {
                console.log('shoecsasafaff');
                var target = $(this.getAttribute('href'));
                $('.header ul').slideUp();
                if (target.length) {
                    event.preventDefault();
                    $('html, body').stop().animate({
                        scrollTop: target.offset().top - 150
                    }, 1000);
                }
            });
            
            /* Menu Dropdown slide */  
            $('.icon-menu').on('click',function(e){
                $('.menu').slideToggle('fast');
                e.preventDefault();
            });
            
            /* Window scroll function */
            $(window).on('scroll',function () {
                
                /* adding background to navbar */
                if ($(window).scrollTop() > 80) {
                    $('.navbar,.header').addClass('navbar-smooth');
                } else {
                    $('.navbar,.header').removeClass('navbar-smooth');
                }
                
                /* adding active style on scroll */             
                $('.navbar ul li a').each(function(){  
                    var scrollPos = $(document).scrollTop();
                    var refElement = $($(this).attr("href"));                    
                    if (refElement.offset().top <= scrollPos + 400 && refElement.offset().top + refElement.height() + $('.bg-seperator-1').height() > scrollPos) {
                        $('.navbar ul li a').removeClass("active");
                        $(this).addClass("active");
                    }
                    else{
                        $(this).removeClass("active");
                    }
                });
            });
            
            /* logo images hover functionality in desktop */
            $(".brndImg").hover(function () {
                if ($(document).width() > 599) {
                    var id = this.id;
                    $('.home').css({'background': "url(images/bg-seperator3.png) no-repeat", 'background-size': "100% 100%"});
                    $(this).children().attr('src', './images/logo-' + id + '-brown.png');
                    $(this).css({'background': 'rgba(255, 255, 255, 0.8)', 'transition': 'background 0.3s ease-in'});
                }              
            }, function () {
                if ($(document).width() > 599) {
                    var id = this.id;
                    $('.home').css({'background': "url(images/bg-banner.jpg) no-repeat", 'background-size': "100% 100%"});
                    $(this).children().attr('src', './images/logo-' + id + '.png');
                    $(this).css({'background': ''});
                }
            });
            
            /* logo images onclick functionality in mobile size */            
            $(".brndImg").click(function (e) {
                if ($(document).width() < 599) {                    
                    var id = this.id;
                    $("#hylife img").attr('src', './images/logo-hylife.png');
                    $("#elixir img").attr('src', './images/logo-elixir.png');
                    $("#bon img").attr('src', './images/logo-bon.png');
                    $("#canoli img").attr('src', './images/logo-canoli.png');
                    $("#currie img").attr('src', './images/logo-currie.png');
                    $(".brndImg").css({'background': 'none'});
                    $(this).children().attr('src', './images/logo-' + id + '-brown.png');
                    $(this).css({'background': 'rgba(255, 255, 255, 0.8)', 'transition': 'background 0.3s ease-in'});
                    e.stopPropagation();
                }                 
            });
            
            //click on document
            $(document).click(function(e){
                //reset brand images in home
                $("#hylife img").attr('src', './images/logo-hylife.png');
                $("#elixir img").attr('src', './images/logo-elixir.png');
                $("#bon img").attr('src', './images/logo-bon.png');
                $("#canoli img").attr('src', './images/logo-canoli.png');
                $("#currie img").attr('src', './images/logo-currie.png');
                $(".brndImg").css({'background': 'none'});
            });
            
            /* Image Flip Desktop*/
            $(".flip").flip({trigger: 'manual'});
            $('.flip').hover(function () {
                //$(".flip").flip({trigger: 'manual'});
                if($(document).width() > 599){
                    $(this).flip(true);
                }
            }, function () {
                if($(document).width() > 599){
                    $(this).flip(false);
                }
            });
            
            /* Image Flip Mobile*/
            $('.flip').click(function () {
                //$(".flip").flip({trigger: 'manual'});
                if($(document).width() < 599){
                    $(this).flip('toggle');
                }
            });
            /* sly slider functinality*/
            setTimeout(function () {
                /* Vertical sly slider */
                $('#slyFrame').sly({                    
                    itemNav: 'basic',                   
                    mouseDragging: 1,
                    touchDragging: 1,
                    releaseSwing: 1,
                    startAt: 0,
                    scrollBar: $('.scrollbar'),
                    scrollBy: 1,
                    elasticBounds: 1,                   
                    dragHandle: 1,                     
                    dynamicHandle: false,                    
                    scrollTrap: true,
                    clickBar: true
                });
                /* Horizontal sly slider */
                $('.dlgFrame').sly({
                    horizontal: 1,
                    itemNav: 'basic',
                    mouseDragging: 1,
                    touchDragging: 1,
                    releaseSwing: 1,
                    startAt: 0,
                    scrollBy: 1,
                    elasticBounds: 1,
                    scrollTrap: false,
                    clickBar: true,
                    speed: 1000,
                    easing: 'swing'
                });
            }, 1000);
            
            // sly reload on resize
            $(window).resize(function() {        
                $('#slyFrame').sly('reload'); 
                $('#basic').sly('reload');
            });
            
            // sly scrollbar show on mousehover
            $('.frame_prnt').hover(function(){
                $('.scrollbar').show();
            },function(){
                $('.scrollbar').hide();
            });
                       
        });   //closed document ready function
        
        /* show and hide in mobile,desktop */
        $scope.$watch(function(){
            $scope.sh_m = $mdMedia('xs');
            $scope.sh_d = $mdMedia('gt-xs');
        });    
        $scope.similarEvents = [
            {sub_hd: 'HYLIFE', main_hd: 'Beer Testing BBQ', time: '8:45 PM'},
            {sub_hd: 'CANNOLI CAFE', main_hd: 'Brown Banking', time: '8:45 PM'},
            {sub_hd: 'ELIXIR', main_hd: 'Pizzarria Itlaia', time: '8:45 PM'},
            {sub_hd: 'HYLIFE', main_hd: 'Beer Testing BBQ', time: '8:45 PM'},
            {sub_hd: 'CANNOLI CAFE', main_hd: 'Brown Banking', time: '8:45 PM'},
            {sub_hd: 'ELIXIR', main_hd: 'Pizzarria Itlaia', time: '8:45 PM'},
            {sub_hd: 'HYLIFE', main_hd: 'Beer Testing BBQ', time: '8:45 PM'},
            {sub_hd: 'CANNOLI CAFE', main_hd: 'Brown Banking', time: '8:45 PM'},
            {sub_hd: 'ELIXIR', main_hd: 'Pizzarria Itlaia', time: '8:45 PM'}
        ];        
        //events dialog open function
        $scope.upcomingEvents = function (ev) {
            $mdDialog.show({
                templateUrl: './partials/eventsTemplate.php',
                clickOutsideToClose: true,
                escapeToClose: true,
                targetEvent: ev,
                disableParentScroll: false,
                controller: 'mkCtrl'
            });
            var id = Math.floor(Math.random() * 10000);
            $(ev.currentTarget).attr("id", id);
            $params = $.param({'id': id});
            $http({
                method: "POST",
                url: "./list.php",
                data: $params
            });
        };
        
        //Dialog cancel
        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        $http.defaults.headers.post["Content-Type"] = 'application/x-www-form-urlencoded; charset=utf-8';
        
        //mobile size date functionality
        $scope.myDate = new Date();
        $scope.dateClick = function () {
            $params = $.param({
                'year': $filter('date')($scope.myDate, 'yyyy'),
                'month': $filter('date')($scope.myDate, 'MM'),
                'day': $filter('date')($scope.myDate, 'dd')
            });
            $http({
                method: "POST",
                url: "./date.php",
                data: $params
            });
        };
        
        //desktop date 
        $scope.options = {
            defaultDate: new Date(),
            dayNamesLength: 1, // 1 for "M", 2 for "Mo", 3 for "Mon"; 9 will show full day names. Default is 1.
            sundayIsFirstDay: true, //set sunday as first day of week. Default is false
            eventClick: function (date) { // called before dateClick and only if clicked day has events
                //console.log(date);
            },
            dateClick: function (date) { // called every time a day is clicked
                //console.log(date);
                $params = $.param({
                    'year': date.year,
                    'month': date.month,
                    'day': date.day
                });
                $http({
                    method: "POST",
                    url: "./date.php",
                    data: $params
                });
            },
            changeMonth: function (month, year) {
                console.log(month, year);
            },
            filteredEventsChange: function (filteredEvents) {
                //console.log(filteredEvents);
            }
        };

        $scope.events = [
        ];
    }]);