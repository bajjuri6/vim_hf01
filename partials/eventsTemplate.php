<md-dialog aria-label="empfrm"  layout="column">
    <div class="md-toolbar-tools">        
        <span flex></span>
        <md-button class="md-icon-button " aria-label="cancel" ng-click="cancel()">
            <md-icon class="md-default-theme" class="material-icons"><img src="images/close.png" class="width_24"></md-icon>
        </md-button>
    </div>
    <md-dialog-content layout='column' flex="100">
        <div layout-margin layout flex>
            <div layout flex="50" class="img-beer-event">
                <img src="images/img-beer-event.png" flex="100">
            </div>
            <div layout="column" flex="50">
                <div class="dialog_header">
                    <span>HYLIFE</span>
                    <h4>Beer Tasting BBQ</h4>
                </div>
                <div layout="column">                    
                    <div layout layout-align="start center" id="btm_drdr">
                        <div layout flex="50" flex-sm="35" layout layout-align="start center">
                            <span><i class="material-icons icon-schedule">&#xE8B5;</i></span>
                            <span class="time">8:45 PM</span>
                        </div>
                        <div flex-offset="5"  class="limited_seats">
                            <ul>
                                <li>Limited Seats</li>
                            </ul>
                        </div>
                    </div>   
                </div>
                <div class="dialog_text">
                    <p>
                        Fruits and vegetables are universally promoted as healthy. The Dietary Guidelines for Americans 2010 recommend you make one-half of your plate fruits and vegetables. Myplate.gov also supports that one-half the plate should be fruits and vegetables. Fruits and vegetables include a diverse group of plant foods that vary greatly in content of energy and nutrients
                    </p>
                </div>
                <div class="register" layout layout-align='start center'>
                    <a href="" class="outline">REGISTER NOW</a>
                </div>
            </div>
            <div flex="10" layout="column" layout-align='start end' class="social_pics">
                <div class="fb cursor_point"  layout layout-align='center center'>
                    <img src="images/facebook.png">                    
                </div>
                <div class="twt cursor_point" layout layout-align='center center'>
                    <img src="images/twitter.png">
                </div>
            </div>
        </div>
        <div class="gallery_dialog" flex>
            <h1 flex-offset="5">Gallery</h1>
            <div class="dlgFrame" id="basic">
                <ul class="dlgSlidee">
                    <li>
                        <img src="./images/img-event1.png">
                    </li>
                    <li>
                        <img src="./images/img-event2.png">
                    </li>
                    <li>
                        <img src="./images/img-event3.png">
                    </li>
                    <li>
                        <img src="./images/img-event4.png">
                    </li>
                    <li>
                        <img src="./images/img-event3.png">
                    </li>
                    <li>
                        <img src="./images/img-event4.png">
                    </li>
                    <li>
                        <img src="./images/img-event2.png">
                    </li>
                    <li>
                        <img src="./images/img-event3.png">
                    </li>
                    <li>
                        <img src="./images/img-event4.png">
                    </li>
                    <li>
                        <img src="./images/img-event3.png">
                    </li>
                    <li>
                        <img src="./images/img-event4.png">
                    </li>
                    <li>
                        <img src="./images/img-event2.png">
                    </li>
                    <li>
                        <img src="./images/img-event3.png">
                    </li>
                    <li>
                        <img src="./images/img-event4.png">
                    </li>
                </ul>                
            </div>            
        </div>
    </md-dialog-content>
</md-dialog>